const user = {
    state: () => ({
        first_name: null, 
        middle_name: null,
        last_name: null,
        email_address: null,
        image: null,
        id: null
    }),
    mutations: { 
        save(state, payload) {
            state.id = payload.id;
            state.first_name = payload.first_name,
            state.middle_name = payload.middle_name,
            state.last_name = payload.last_name,
            state.email_address = payload.email_address,
            state.image = payload.image
        },
        delete(state) {
            state.id = null;
            state.first_name = null,
            state.middle_name = null,
            state.last_name = null,
            state.email_address = null,
            state.image = null
        },
        saveProfilePhoto(state, payload) {
            state.image = payload;
        }
    },
    actions: { 

    },
    getters: { 

    },
    namespaced: true
}
export default user
