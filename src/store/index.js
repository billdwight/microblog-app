import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";
import user from '@/store/user'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLoggedIn: false
  },
  mutations: {
    login (state, payload) {
      state.isLoggedIn = true;
      this.commit('user/save', payload);
    },
    logout(state) {
      state.isLoggedIn = false;
      this.commit('user/delete');
    }
  },
  actions: {
  },
  modules: {
    user: user
  },
  plugins: [
    createPersistedState()
  ]
})
