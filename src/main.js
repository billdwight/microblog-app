import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './quasar'

Vue.config.productionTip = false

// Load axios.
import axios from 'axios'

// Make the default client to Axios.
Vue.prototype.$http = axios;
console.log('The HTTP client has been changed to AXIOS.');

// Load the baseURL from the ENV file.
Vue.prototype.$http.defaults.baseURL = process.env.VUE_APP_SERVER;
console.log('The API endpoint ' + Vue.prototype.$http.defaults.baseURL + ' has been assigned.');

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
